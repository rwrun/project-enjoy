var gulp 		= require('gulp'),
	sass 		= require('gulp-sass'),
	browserSync = require('browser-sync'),
	slim 		= require('gulp-slim'),
	plugins 	= require('gulp-load-plugins')({scope: ['devDependencies']}),
	gutil 		= require('gulp-util'),
	sassLint = require('gulp-sass-lint');


gulp.task('sassLint', function () {
  return gulp.src('app/sass/**/*.s+(a|c)ss')
    .pipe(sassLint())
    .pipe(sassLint.format())
    .pipe(sassLint.failOnError())
});
gulp.task('sass', function(){
	return gulp.src('app/sass/**/*.s+(a|c)ss')
	.pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
	.pipe(gulp.dest('app/css/'))
	.pipe(browserSync.reload({stream: true}))
});

gulp.task('slim', function(){
	return gulp.src("app/slim/*.slim")
	.pipe(plugins.slim({
		pretty: true
	}).on('error', gutil.log))
	.pipe(gulp.dest("app/"))
	.pipe(browserSync.reload({stream: true}));
});

gulp.task('browser-sync', function(){
	browserSync({
		server: {
			baseDir: 'app/'
		},
		notify: false
	});
});
gulp.task('watch', ['browser-sync', 'sass', 'slim', 'sassLint'], function(){
	gulp.watch('app/sass/**/*.s+(a|c)ss', ['sass']);
	//gulp.watch('app/*.html', browserSync.reload);
	gulp.watch('app/js/**/*.js', browserSync.reload);
	gulp.watch('app/slim/*.slim', ['slim'])
});

